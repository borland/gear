from logging import getLogger

from flask import Blueprint, jsonify


api_name = 'user'
url_prefix = '/{}'.format(api_name)
blueprint = Blueprint(api_name, __name__, url_prefix=url_prefix)

log = getLogger(__name__)


@blueprint.route('/get', methods=['GET'])
def user_get():
    return jsonify({'status': 'ok'})
