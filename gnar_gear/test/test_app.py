import logging
import os
import sys
from datetime import timedelta
from importlib import reload
from json import dumps
from pkgutil import get_loader
from unittest.mock import Mock

import pytest
from flask import Flask

from gnar_gear import gnar_app
from gnar_gear.constants import PASSWORD_CANNOT_BE_EMPTY, TOKEN_HAS_EXPIRED
from gnar_gear.test.constants import ARGON2_DEFAULTS, ENVIRONMENT, TEST_COMPLEX_PASSWORD, TEST_SIMPLE_PASSWORD

log = logging.getLogger()


class TestApp:

    @pytest.fixture
    def mock_app(self, monkeypatch):
        monkeypatch.setattr('argparse.ArgumentParser.parse_args',
                            lambda *a, **kw: type('args', (object,), {'port': ''}))
        monkeypatch.setattr('flask.Flask.run', lambda *a, **kw: None)
        monkeypatch.setattr('bjoern.run', lambda *a, **kw: None)
        monkeypatch.setattr('os.environ', ENVIRONMENT)
        monkeypatch.setattr('postgres.Postgres.__init__', lambda *a, **kw: None)
        reload(gnar_app)
        yield gnar_app.GnarApp('gnar_gear', production=False, port=9400)

    def test_app_settings(self, mock_app):
        mock_app.run()
        root = logging.getLogger()
        app_handler = root.handlers[-1]
        assert isinstance(mock_app.flask, Flask)
        assert isinstance(app_handler, logging.StreamHandler)
        assert hasattr(mock_app, 'argon2')
        assert hasattr(mock_app, 'db')
        assert hasattr(mock_app, 'jwt')

    def test_app_no_settings(self, mock_app):
        mock_app.no_db = True
        mock_app.no_jwt = True
        mock_app.run()
        assert not hasattr(mock_app, 'db')
        assert not hasattr(mock_app, 'jwt')

    def test_app_get_ses_client(self, monkeypatch, mock_app):
        mock = Mock()
        monkeypatch.setattr('boto3.Session.client', mock)
        mock_app.run()
        mock_app.get_ses_client()
        assert mock.call_count == 1

    def test_app_log_level(self, monkeypatch, mock_app):
        mock_app.run()
        assert log.level == logging.INFO
        env = ENVIRONMENT.copy()
        env['GNAR_LOG_LEVEL'] = 'WARNING'
        monkeypatch.setattr('os.environ', env)
        mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400)
        mock_app.run()
        assert log.level == logging.WARNING
        env['GNAR_GNAR_GEAR_LOG_LEVEL'] = 'ERROR'
        monkeypatch.setattr('os.environ', env)
        mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400)
        mock_app.run()
        assert log.level == logging.ERROR
        mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400, log_level='CRITICAL')
        mock_app.run()
        assert log.level == logging.CRITICAL

    def test_app_argon2(self, monkeypatch, mock_app):
        mock_app.run()
        with pytest.raises(Exception) as e:
            mock_app.generate_password_hash('')
        assert PASSWORD_CANNOT_BE_EMPTY in str(e)
        test_hash = mock_app.generate_password_hash(TEST_SIMPLE_PASSWORD)
        assert mock_app.check_password_hash(test_hash, TEST_SIMPLE_PASSWORD)
        test_hash = mock_app.generate_password_hash(TEST_COMPLEX_PASSWORD)
        assert mock_app.check_password_hash(test_hash, TEST_COMPLEX_PASSWORD)
        assert not mock_app.check_password_hash(test_hash, TEST_SIMPLE_PASSWORD)
        test_hash = mock_app.argon2.hash(TEST_SIMPLE_PASSWORD)
        assert mock_app.argon2.verify(test_hash, TEST_SIMPLE_PASSWORD)
        test_hash = mock_app.argon2.hash(TEST_COMPLEX_PASSWORD)
        assert mock_app.argon2.verify(test_hash, TEST_COMPLEX_PASSWORD)
        with pytest.raises(Exception) as e:
            mock_app.argon2.verify(test_hash, TEST_SIMPLE_PASSWORD)
        assert 'argon2.exceptions.VerifyMismatchError' in str(e)
        for key, value in ARGON2_DEFAULTS.items():
            assert getattr(mock_app.argon2, key) == value
        test_params = {key: 'ascii' if isinstance(val, str) else val * 2 for key, val in ARGON2_DEFAULTS.items()}
        test_environment = {'{}_{}'.format('gnar_argon2', key).upper(): val for key, val in test_params.items()}
        monkeypatch.setattr('os.environ', {**ENVIRONMENT, **test_environment})
        mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400)
        mock_app.run()
        for key, value in test_params.items():
            assert getattr(mock_app.argon2, key) == value

    def test_app_blueprints(self, monkeypatch, mock_app):
        mock_app.run()
        assert 'user' in mock_app.flask.blueprints
        mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400, blueprint_modules='user')
        mock_app.run()
        assert 'user' in mock_app.flask.blueprints
        mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400, blueprint_modules=['user'])
        mock_app.run()
        assert 'user' in mock_app.flask.blueprints
        mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400, blueprint_modules='user.apis')
        mock_app.run()
        assert 'user' in mock_app.flask.blueprints
        mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400, blueprint_modules=['user.apis'])
        mock_app.run()
        assert 'user' in mock_app.flask.blueprints
        mock_app = gnar_app.GnarApp('gnar_gear', production=True, port=9400)
        mock_app.run()
        assert 'user' in mock_app.flask.blueprints
        mock_app = gnar_app.GnarApp('gnar_gear', production=True, port=9400, blueprint_modules='user')
        mock_app.run()
        assert 'user' in mock_app.flask.blueprints
        app_root = os.path.dirname(get_loader('gnar_gear').path)
        test_filename = os.path.join(app_root, 'app', 'user', '__test_failure__.py')
        try:
            with open(test_filename, 'w') as test_file:
                test_file.write('this_will_fail()\n')
            mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400)
            mock_app.run()
            assert 'user' in mock_app.flask.blueprints
            mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400,
                                        blueprint_modules=['not_a_module', 'user'])
            mock_app.run()
            assert 'user' in mock_app.flask.blueprints
        finally:
            os.remove(test_filename)
        test_filename1 = os.path.join(app_root, 'app', 'user', '__test_importer__.py')
        test_filename2 = os.path.join(app_root, 'app', 'user', '__test_failure__.py')
        try:
            with open(test_filename1, 'w') as test_file:
                test_file.write('import gnar_gear.app.user.__test_failure__\n')
            with open(test_filename2, 'w') as test_file:
                test_file.write('import sys\n')
                test_file.write('this_will_fail()\n')
            mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400)
            mock_app.run()
            assert 'user' in mock_app.flask.blueprints
            mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400)
            mock_app.run()
            assert 'user' in mock_app.flask.blueprints
        finally:
            os.remove(test_filename1)
            os.remove(test_filename2)
        try:
            with open(test_filename1, 'w') as test_file:
                test_file.write('from gnar_gear.app.user.__test_failure__ import x\n')
            with open(test_filename2, 'w') as test_file:
                test_file.write('def x\n')  # intentional syntax error
            mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400)
            module = sys.modules.get('gnar_gear.gnar_app')
            sys.modules.pop('gnar_gear.gnar_app', None)
            mock_app.run()
            assert 'user' in mock_app.flask.blueprints
            mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400)
            mock_app.run()
            assert 'user' in mock_app.flask.blueprints
            if module:
                sys.modules['gnar_gear.gnar_app'] = module
        finally:
            os.remove(test_filename1)
            os.remove(test_filename2)

    def test_app_config_token_expiration(self, monkeypatch, mock_app):
        mock_app.run()
        assert mock_app.flask.config['JWT_ACCESS_TOKEN_EXPIRES'] == timedelta(minutes=30)
        env = ENVIRONMENT.copy()
        del env['GNAR_JWT_ACCESS_TOKEN_EXPIRES_MINUTES']
        monkeypatch.setattr('os.environ', env)
        reload(gnar_app)
        mock_app.run()
        assert mock_app.flask.config['JWT_ACCESS_TOKEN_EXPIRES'] == timedelta(minutes=15)

    def test_app_error_handler(self, mock_app):
        mock_app.run()
        with mock_app.flask.app_context():
            error = mock_app.flask.error_handler_spec[None][None][Exception](Exception('Test Exception'))
            assert error.status_code == 200
            assert error.json['error'] == 'GnarGear API Error'
            assert 'traceback' in error.json

    def test_app_jwt(self, monkeypatch, mock_app):
        mock_app.run()
        with mock_app.flask.app_context():
            error = mock_app.jwt._expired_token_callback()
            assert error.status_code == 200
            assert error.json['error'] == TOKEN_HAS_EXPIRED
            message = 'Invalid token'
            error = mock_app.jwt._invalid_token_callback(message)
            assert error.status_code == 200
            assert error.json['error'] == message
            message = 'Unauthorized'
            error = mock_app.jwt._unauthorized_callback(message)
            assert error.status_code == 200
            assert error.json['error'] == message
        with pytest.raises(Exception) as e:
            env = ENVIRONMENT.copy()
            del env['GNAR_JWT_SECRET_KEY']
            monkeypatch.setattr('os.environ', env)
            mock_app = gnar_app.GnarApp('gnar_gear', production=False, port=9400)
            mock_app.run()
        assert 'ValueError: Environment variable "GNAR_JWT_SECRET_KEY" does not exist.' in str(e)

    def test_app_after_request(self, monkeypatch, mock_app):
        mock_jwt_token = 'JWT Token'
        mock_app.run()
        with mock_app.flask.app_context():
            response = mock_app.flask.after_request_funcs[None][0](type('response', (object,), {'headers': {}}))
            assert response.headers['Access-Control-Allow-Origin'] == '*'
            assert response.headers['Access-Control-Allow-Methods'] == 'GET, POST, PUT'
            assert response.headers['Access-Control-Allow-Headers'] == 'Authorization'
            assert response.headers['Access-Control-Expose-Headers'] == 'Authorization'
            assert 'Authorization' not in response.headers
        with monkeypatch.context() as m:
            m.setattr('flask_jwt_extended.get_jwt_identity', lambda: 'Authenticated User')
            m.setattr('flask_jwt_extended.create_access_token', lambda identity: mock_jwt_token)
            reload(gnar_app)
            mock_app.run()
            with mock_app.flask.app_context():
                response = mock_app.flask.after_request_funcs[None][0](type('response', (object,), {'headers': {}}))
                assert response.headers['Access-Control-Allow-Origin'] == '*'
                assert response.headers['Access-Control-Allow-Methods'] == 'GET, POST, PUT'
                assert response.headers['Access-Control-Allow-Headers'] == 'Authorization'
                assert response.headers['Access-Control-Expose-Headers'] == 'Authorization'
                assert response.headers['Authorization'] == 'Bearer {}'.format(mock_jwt_token)
            mock_app.no_jwt = True
            mock_app.run()
            with mock_app.flask.app_context():
                response = mock_app.flask.after_request_funcs[None][0](type('response', (object,), {'headers': {}}))
                assert 'Authorization' not in response.headers
            mock_app.no_jwt = False
        reload(gnar_app)
        mock_app.production = True
        mock_app.run()
        with mock_app.flask.app_context():
            response = mock_app.flask.after_request_funcs[None][0](type('response', (object,), {'headers': {}}))
            assert 'Access-Control-Allow-Origin' not in response.headers
            assert 'Access-Control-Allow-Methods' not in response.headers
            assert 'Access-Control-Allow-Headers' not in response.headers
            assert 'Access-Control-Expose-Headers' not in response.headers
            assert 'Authorization' not in response.headers
        with monkeypatch.context() as m:
            m.setattr('flask_jwt_extended.get_jwt_identity', lambda: 'Authenticated User')
            m.setattr('flask_jwt_extended.create_access_token', lambda identity: mock_jwt_token)
            reload(gnar_app)
            mock_app.production = True
            mock_app.run()
            with mock_app.flask.app_context():
                response = mock_app.flask.after_request_funcs[None][0](type('response', (object,), {'headers': {}}))
                assert 'Access-Control-Allow-Origin' not in response.headers
                assert 'Access-Control-Allow-Methods' not in response.headers
                assert 'Access-Control-Allow-Headers' not in response.headers
                assert 'Access-Control-Expose-Headers' not in response.headers
                assert response.headers['Authorization'] == 'Bearer {}'.format(mock_jwt_token)
            mock_app.no_jwt = True
            mock_app.run()
            with mock_app.flask.app_context():
                response = mock_app.flask.after_request_funcs[None][0](type('response', (object,), {'headers': {}}))
                assert 'Authorization' not in response.headers

    def test_peer(self, monkeypatch, mock_app):
        mock_app.run()
        with mock_app.flask.app_context():
            mock = Mock()
            data = {'some': 'thing'}
            json = dumps({'json': 'string'})
            with monkeypatch.context() as m:
                m.setattr('requests.request', mock)
                mock_app.peer('off-piste').request('get', 'check-in')
                mock.assert_called_with('get', 'http://127.0.0.1:9401/check-in')
                m.setattr('requests.head', mock)
                mock_app.peer('off-piste').head('check-in')
                mock.assert_called_with('http://127.0.0.1:9401/check-in')
                m.setattr('requests.get', mock)
                mock_app.peer('off-piste').get('check-in')
                mock.assert_called_with('http://127.0.0.1:9401/check-in', None)
                m.setattr('requests.post', mock)
                mock_app.peer('off-piste').post('check-in', data)
                mock.assert_called_with('http://127.0.0.1:9401/check-in', data, None)
                mock_app.peer('off-piste').post('check-in', data, json)
                mock.assert_called_with('http://127.0.0.1:9401/check-in', data, json)
                m.setattr('requests.put', mock)
                data = {'some': 'thing'}
                mock_app.peer('off-piste').put('check-in', data)
                mock.assert_called_with('http://127.0.0.1:9401/check-in', data)
                m.setattr('requests.patch', mock)
                data = {'some': 'thing'}
                mock_app.peer('off-piste').patch('check-in', data)
                mock.assert_called_with('http://127.0.0.1:9401/check-in', data)
                m.setattr('requests.delete', mock)
                mock_app.peer('off-piste').delete('check-in')
                mock.assert_called_with('http://127.0.0.1:9401/check-in')
                m.setattr('flask_jwt_extended.get_jwt_identity', lambda: 'admin')
                reload(gnar_app)
                mock_app.peer('off-piste').request('get', 'check-in', auto_auth=True)
                assert mock.call_args[0] == ('get', 'http://127.0.0.1:9401/check-in')
                assert 'Authorization' in mock.call_args[1]['headers']
                mock_app.peer('off-piste').head('check-in', auto_auth=True)
                assert mock.call_args[0] == ('http://127.0.0.1:9401/check-in',)
                assert 'Authorization' in mock.call_args[1]['headers']
                mock_app.peer('off-piste').get('check-in', auto_auth=True)
                assert mock.call_args[0] == ('http://127.0.0.1:9401/check-in', None)
                assert 'Authorization' in mock.call_args[1]['headers']
                mock_app.peer('off-piste').post('check-in', data, auto_auth=True)
                assert mock.call_args[0] == ('http://127.0.0.1:9401/check-in', data, None)
                assert 'Authorization' in mock.call_args[1]['headers']
                mock_app.peer('off-piste').post('check-in', data, json, auto_auth=True)
                assert mock.call_args[0] == ('http://127.0.0.1:9401/check-in', data, json)
                assert 'Authorization' in mock.call_args[1]['headers']
                mock_app.peer('off-piste').put('check-in', data, auto_auth=True)
                assert mock.call_args[0] == ('http://127.0.0.1:9401/check-in', data)
                assert 'Authorization' in mock.call_args[1]['headers']
                mock_app.peer('off-piste').patch('check-in', data, auto_auth=True)
                assert mock.call_args[0] == ('http://127.0.0.1:9401/check-in', data)
                assert 'Authorization' in mock.call_args[1]['headers']
                mock_app.peer('off-piste').delete('check-in', auto_auth=True)
                assert mock.call_args[0] == ('http://127.0.0.1:9401/check-in',)
                assert 'Authorization' in mock.call_args[1]['headers']
            with pytest.raises(Exception) as e:
                mock_app.peer('not-a-service')
            assert 'Environment variable "GNAR_NOT_A_SERVICE_SERVICE_PORT" does not exist.' in str(e)
            mock_app.production = True
            with pytest.raises(Exception) as e:
                mock_app.peer('not-a-service')
            assert 'Environment variable "NOT_A_SERVICE_SERVICE_PORT" does not exist.' in str(e)

    def test_external(self, monkeypatch, mock_app):
        mock_app.run()
        mock = Mock()
        with monkeypatch.context() as m:
            m.setattr('requests.request', mock)
            mock_app.external.request('get', 'http://test.api.com')
            mock.assert_called_with('get', 'http://test.api.com')

    def test_configure_sqs_poller(self, monkeypatch, mock_app):
        with pytest.raises(Exception) as e:
            gnar_app.GnarApp('gnar_gear', production=False, port=9400, sqs={'x': 'x'}).run()
        assert 'ValueError: "queue_name" missing in {"x": "x"}' in str(e)
        with pytest.raises(Exception) as e:
            gnar_app.GnarApp('gnar_gear', production=False, port=9400, sqs={'queue_name': 'test'}).run()
        assert 'ValueError: "callback" missing in {"queue_name": "test"}' in str(e)
        with monkeypatch.context() as m:
            mock_receive_message = Mock(return_value={})
            mock_client = type('sqs_client', (object,), {'get_queue_url': lambda *a, **kw: {'QueueUrl': 'mock_url'},
                                                         'receive_message': mock_receive_message})
            mock_timer = Mock()
            m.setattr('boto3.Session.client', lambda *a, **kw: mock_client)
            m.setattr('threading.Timer', mock_timer)
            reload(gnar_app)
            callback_mock = Mock()
            sqs = {'queue_name': 'test', 'callback': callback_mock}
            gnar_app.GnarApp('gnar_gear', production=False, port=9400, sqs=sqs).run()
            mock_receive_message.assert_called_with(QueueUrl='mock_url')
            args = mock_timer.call_args[0]
            assert args[0] == 60
            print(dir(args[1]))
            assert args[1].__func__ == gnar_app.PerpetualTimer.handle_function
            mock_receive_message = Mock(side_effect=[{'Messages': [{'ReceiptHandle': 42}, {'ReceiptHandle': 420}]}, {}])
            mock_delete_message = Mock()
            mock_client = type('sqs_client', (object,), {'get_queue_url': lambda *a, **kw: {'QueueUrl': 'mock_url'},
                                                         'receive_message': mock_receive_message,
                                                         'delete_message': mock_delete_message})
            gnar_app.GnarApp('gnar_gear', production=False, port=9400, sqs=sqs).run()
            assert callback_mock.call_count == 2
            assert mock_delete_message.called_with(42, 420)
            callback_mock = Mock(side_effect=[True, False, None])
            sqs = {'queue_name': 'test', 'callback': callback_mock}
            mock_receive_message = Mock(side_effect=[
                {'Messages': [{'ReceiptHandle': 1}, {'ReceiptHandle': 2}, {'ReceiptHandle': 3}]}, {}])
            mock_delete_message = Mock()
            mock_client = type('sqs_client', (object,), {'get_queue_url': lambda *a, **kw: {'QueueUrl': 'mock_url'},
                                                         'receive_message': mock_receive_message,
                                                         'delete_message': mock_delete_message})
            gnar_app.GnarApp('gnar_gear', production=False, port=9400, sqs=sqs).run()
            assert callback_mock.call_count == 3
            assert mock_delete_message.called_with(2)
            sqs = {'queue_name': 'Wang Chung',
                   'callback': callback_mock,
                   'attribute_names': 'Everybody Have Fun Tonight',
                   'message_attribute_names': 'Hypnotize Me',
                   'max_number_of_messages': 1982,
                   'visibility_timeout': 10,
                   'wait_time_seconds': 11,
                   'receive_request_attempt_id': 'Fire in the Twilight'}
            mock_receive_message = Mock(return_value={})
            mock_client = type('sqs_client', (object,), {'get_queue_url': lambda **kw: {'QueueUrl': 'Dance Hall Days'},
                                                         'receive_message': mock_receive_message})
            gnar_app.GnarApp('gnar_gear', production=False, port=9400, sqs=sqs).run()
            args = mock_receive_message.call_args[0]
            mock_receive_message.assert_called_with(QueueUrl='Dance Hall Days',
                                                    AttributeNames='Everybody Have Fun Tonight',
                                                    MessageAttributeNames='Hypnotize Me',
                                                    MaxNumberOfMessages=1982,
                                                    VisibilityTimeout=10,
                                                    WaitTimeSeconds=11,
                                                    ReceiveRequestAttemptId='Fire in the Twilight')

    def test_send_sqs_message(self, monkeypatch, mock_app):
        with monkeypatch.context() as m:
            mock_send_message = Mock()
            mock_client = type('sqs_client', (object,), {'get_queue_url': lambda *a, **kw: {'QueueUrl': 'China Girl'},
                                                         'send_message': mock_send_message})
            m.setattr('boto3.Session.client', lambda *a, **kw: mock_client)
            mock_app.send_sqs_message('David Bowie', 'Space Oddity')
            mock_send_message.assert_called_with(QueueUrl='China Girl', MessageBody='Space Oddity')
            kwargs = {'delay_seconds': 1,
                      'message_attributes': 'Ziggy Stardust',
                      'message_deduplication_id': 1984,
                      'message_group_id': 2001}
            mock_app.send_sqs_message('David Bowie', 'Space Oddity', **kwargs)
            mock_send_message.assert_called_with(QueueUrl='China Girl',
                                                 MessageBody='Space Oddity',
                                                 DelaySeconds=1,
                                                 MessageAttributes='Ziggy Stardust',
                                                 MessageDeduplicationId=1984,
                                                 MessageGroupId=2001)
